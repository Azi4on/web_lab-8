import { BASE_API_URL } from '../common/constants';
import { Comment, Post, User } from '../common/types';

export const getManyPosts = async (): Promise<Post[]> => {
  const res = await fetch(BASE_API_URL + 'posts', { method: 'GET' });
  const posts = await res.json();

  return posts;
};

export const getSinglePost;

export const getPostComments;

export const getSingleUser;
